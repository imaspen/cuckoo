// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "CuckooGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class CUCKOO_API ACuckooGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
protected:
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable)
	virtual void EndGame();
	
	UFUNCTION(BlueprintCallable)
	void StartGame();

public:
	virtual void StartToLeaveMap() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTimerHandle Timer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FTimerHandle StartTimer;
};
