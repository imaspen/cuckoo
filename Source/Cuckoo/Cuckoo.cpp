// Fill out your copyright notice in the Description page of Project Settings.

#include "Cuckoo.h"
#include "Modules/ModuleManager.h"

#include "CuckooModule.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FCuckooModule, Cuckoo, "Cuckoo" );
