// Fill out your copyright notice in the Description page of Project Settings.


#include "CuckooGameModeBase.h"

#include <Kismet/GameplayStatics.h>
#include <Engine/World.h>
#include <TimerManager.h>
#include <Engine/Engine.h>

void ACuckooGameModeBase::BeginPlay()
{
	Super::BeginPlay();
	for (int i = 0; i < 3; i++) UGameplayStatics::CreatePlayer(GetWorld());
	GetWorldTimerManager().SetTimer(
		Timer, this, &ACuckooGameModeBase::EndGame, 60.0f, false, 60.0f
	);
	GetWorldTimerManager().PauseTimer(Timer);
	GetWorldTimerManager().SetTimer(
		StartTimer, this, &ACuckooGameModeBase::StartGame, 2.0f, false, 2.0f
	);
	for (int i = 0; i < 4; i++) UGameplayStatics::GetPlayerControllerFromID(GetWorld(), i)
		->SetIgnoreMoveInput(true);
}

void ACuckooGameModeBase::StartGame()
{
	GetWorldTimerManager().UnPauseTimer(Timer);
	for (int i = 0; i < 4; i++) UGameplayStatics::GetPlayerControllerFromID(GetWorld(), i)
		->SetIgnoreMoveInput(false);
}

void ACuckooGameModeBase::EndGame()
{
	UGameplayStatics::OpenLevel(GetWorld(), TEXT("/Game/Maps/Lose"));
}

void ACuckooGameModeBase::StartToLeaveMap()
{
	for (int i = 1; i < 4; i++)
		UGameplayStatics::RemovePlayer(
			UGameplayStatics::GetPlayerControllerFromID(GetWorld(), i), true
		);
}
