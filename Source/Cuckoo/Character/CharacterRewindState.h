#pragma once

#include "CoreMinimal.h"

#include "CharacterRewindState.generated.h"

USTRUCT(BlueprintType)
struct FCharacterRewindState
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FRotator ControlRotation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTransform Transform;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector Velocity;

	FCharacterRewindState()
	{
		this->ControlRotation = FRotator();
		this->Transform = FTransform();
		this->Velocity = FVector(0.0f);
	}

	FCharacterRewindState(FRotator ControlRotation, FTransform Transform, FVector Velocity)
	{
		this->ControlRotation = ControlRotation;
		this->Transform = Transform;
		this->Velocity = Velocity;
	}

	FCharacterRewindState Lerp(FCharacterRewindState OtherState, float Alpha)
	{
		auto otherTransform = OtherState.Transform;
		return FCharacterRewindState(
			FMath::Lerp(ControlRotation, OtherState.ControlRotation, Alpha),
			FTransform(
				FMath::Lerp(Transform.Rotator(), otherTransform.Rotator(), Alpha),
				FMath::Lerp(Transform.GetTranslation(), otherTransform.GetTranslation(), Alpha),
				FMath::Lerp(Transform.GetScale3D(), otherTransform.GetScale3D(), Alpha)
			),
			FMath::Lerp(Velocity, OtherState.Velocity, Alpha)
		);
	}
};