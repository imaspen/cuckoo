// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "CuckooCharacterMovementComponent.generated.h"

UENUM(BlueprintType)
enum class ECustomMovementModeEnum : uint8
{
	CMME_Sprint	UMETA(DisplayName="Sprint")
};

UCLASS()
class CUCKOO_API UCuckooCharacterMovementComponent : public UCharacterMovementComponent
{
	GENERATED_BODY()
public:
	UCuckooCharacterMovementComponent();

	virtual ECustomMovementModeEnum GetCustomMovementMode();

	virtual void SetCustomMovementMode(ECustomMovementModeEnum CustomMovementModeEnum);

protected:
	virtual void PhysCustom(float DeltaTime, int32 Iterations) override;

	virtual void PhysSprint(float DeltaTime, int32 Iterations);
};
