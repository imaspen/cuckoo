// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"

#include "CharacterRewindState.h"

#include "CuckooCharacter.generated.h"

UCLASS()
class CUCKOO_API ACuckooCharacter : public ACharacter
{
	GENERATED_BODY()
public:
	// Sets default values for this character's properties
	ACuckooCharacter(const FObjectInitializer & objectInitializer);

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintCallable)
	virtual void Peck();

	UFUNCTION(BlueprintCallable)
	virtual void ToggleSprint();
	UFUNCTION(BlueprintCallable)
	virtual void StartSprint();
	UFUNCTION(BlueprintCallable)
	virtual void StopSprint();

	UFUNCTION(BlueprintCallable)
	void FireRewind();
	UFUNCTION(BlueprintCallable)
	virtual void StartRewind();
	UFUNCTION(BlueprintCallable)
	virtual void StopRewind();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
	virtual void RecordMovementState();

	UFUNCTION()
	virtual void TickRewind(float DeltaTime);

public:	
	UPROPERTY(VisibleAnywhere)
	class UCameraComponent * CameraComponent;

	UPROPERTY(VisibleAnywhere)
	class USpringArmComponent * SpringArmComponent;

	UPROPERTY(VisibleAnywhere)
	class USphereComponent * PeckCollisionComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float PeckCooldownDuration;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float PeckForce;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float SprintCooldownDuration;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float PeckCooldown;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float SprintCooldown;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bSprinting;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float RewindDuration;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float RewindCooldownDuration;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float RewindSpeedModifier;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float RewindCooldown;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bRewinding;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bPecking;

protected:
	class TDoubleLinkedList<FCharacterRewindState> * RewindStates;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTimerHandle RecordMovementStateHandle;

private:
	int _rewindStatesMaxSize;

	float _rewindTime;

	float _lastFrameNumber;
};
