// Fill out your copyright notice in the Description page of Project Settings.


#include "CuckooCharacter.h"

#include <Camera/CameraComponent.h>
#include <Components/SphereComponent.h>
#include <Containers/List.h> 
#include <GameFramework/SpringArmComponent.h>
#include <TimerManager.h>

#include "CuckooPlayerController.h"
#include "CuckooCharacterMovementComponent.h"

// Sets default values
ACuckooCharacter::ACuckooCharacter(const FObjectInitializer & objectInitializer)
	: Super(objectInitializer.SetDefaultSubobjectClass<UCuckooCharacterMovementComponent>(CharacterMovementComponentName))
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create components
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComponent"));
	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArmComponent"));
	PeckCollisionComponent = CreateDefaultSubobject<USphereComponent>(TEXT("PeckCollisionComponent"));

	// Setup attachments
	SpringArmComponent->SetupAttachment(RootComponent);
	CameraComponent->SetupAttachment(SpringArmComponent);
	PeckCollisionComponent->SetupAttachment(RootComponent);

	// Set component defaults
	SpringArmComponent->TargetOffset = FVector(0.0f, 0.0f, 200.f);
	SpringArmComponent->TargetArmLength = 500.0f;
	SpringArmComponent->bEnableCameraLag = true;

	CameraComponent->SetRelativeRotation(FQuat(FVector(0.0, 1.0f, 0.0f), 20.0f * 0.01745329252f));
	
	PeckCollisionComponent->SetRelativeLocation(FVector(50.0f, 0.0f, 30.0f));

	// Set class defaults
	PeckCooldownDuration = 2.0f;
	SprintCooldownDuration = 5.0f;
	bSprinting = false;
	RewindDuration = 3.0f;
	RewindCooldownDuration = 20.0f;
	RewindSpeedModifier = 2.0f;
	PeckForce = 1.0f;
}

// Called when the game starts or when spawned
void ACuckooCharacter::BeginPlay()
{
	Super::BeginPlay();
	SprintCooldown = SprintCooldownDuration;
	_rewindStatesMaxSize = FMath::FloorToInt(RewindDuration * 100.0f);
	RewindStates = new TDoubleLinkedList<FCharacterRewindState>();
	GetWorldTimerManager().SetTimer(
		RecordMovementStateHandle, this, &ACuckooCharacter::RecordMovementState, 0.1f, true
	);
}

void ACuckooCharacter::RecordMovementState()
{
	auto movementComponent = Cast<UCuckooCharacterMovementComponent>(GetMovementComponent());
	if (!movementComponent) return;
	auto currentState = FCharacterRewindState(
		GetControlRotation(), GetActorTransform(), movementComponent->Velocity
	);
	RewindStates->AddHead(currentState);
	while (RewindStates->Num() > _rewindStatesMaxSize) 
		RewindStates->RemoveNode(RewindStates->GetTail());
}

void ACuckooCharacter::TickRewind(float DeltaTime)
{
	if (!bRewinding) return;

	// Increase rewind time and run stop function if done.
	_rewindTime += DeltaTime;
	if (_rewindTime * RewindSpeedModifier > RewindDuration)
	{
		StopRewind();
		return;
	}

	// Exit if necessary components aren't available
	auto movementComponent = GetMovementComponent();
	if (!movementComponent) return;
	if (!Controller) return;

	// Get the frame number and part
	float frameNumber;
	auto frameFraction = FMath::Modf(_rewindTime * 10.0f * RewindSpeedModifier, &frameNumber);

	// Drop frames we've rewound past
	while (_lastFrameNumber++ < frameNumber) RewindStates->RemoveNode(RewindStates->GetHead());
	_lastFrameNumber = frameNumber;
	
	// Exit if there's nothing to rewind too
	if (RewindStates->Num() == 0) {
		StopRewind();
		return;
	}

	// Get the state to rewind too
	auto currentFrame = RewindStates->GetHead()->GetValue();
	FCharacterRewindState lerpedFrame;
	if (RewindStates->Num() == 1)
	{
		lerpedFrame = currentFrame;
	}
	else
	{
		auto nextFrame = RewindStates->GetHead()->GetNextNode()->GetValue();
		lerpedFrame = currentFrame.Lerp(nextFrame, frameFraction);
	}

	// Perform the rewind
	Controller->SetControlRotation(lerpedFrame.ControlRotation);
	SetActorTransform(lerpedFrame.Transform, false, nullptr, ETeleportType::TeleportPhysics);
	movementComponent->Velocity = lerpedFrame.Velocity;
}

// Called every frame
void ACuckooCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (PeckCooldown > 0.0f) PeckCooldown -= DeltaTime;
	if (RewindCooldown > 0.0f) RewindCooldown -= DeltaTime;

	if (bSprinting)
	{
		SprintCooldown -= DeltaTime;
		if (SprintCooldown < 0) StopSprint();
		else if (GetMovementComponent()->Velocity.IsNearlyZero()) StopSprint();
	}
	else
	{
		SprintCooldown += DeltaTime;
	}
	SprintCooldown = FMath::Clamp(SprintCooldown, 0.0f, SprintCooldownDuration);

	TickRewind(DeltaTime);
}

// Called to bind functionality to input
void ACuckooCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void ACuckooCharacter::Peck()
{
	if (PeckCooldown > 0) return;
	bPecking = true;
	PeckCooldown = PeckCooldownDuration;

	TSet<AActor *> overlappingActors;
	PeckCollisionComponent->GetOverlappingActors(
		overlappingActors, ACuckooCharacter::StaticClass()
	);
	
	for (auto otherPlayer : overlappingActors)
	{
		if (otherPlayer == this) continue;
		UE_LOG(LogTemp, Warning, TEXT("Ouch!"));

		auto direction = otherPlayer->GetActorLocation() - GetActorLocation();
		direction.Z += 10.0f;
		direction.Normalize();

		auto otherCuckoo = Cast<ACuckooCharacter>(otherPlayer);
		otherCuckoo->LaunchCharacter(direction * 500.0f * PeckForce, false, false);
	}
}

void ACuckooCharacter::ToggleSprint()
{
	if (!bSprinting) StartSprint();
	else StopSprint();
}

void ACuckooCharacter::StartSprint()
{
	if (SprintCooldown < SprintCooldownDuration) return;
	bSprinting = true;
	auto movementComponent = Cast<UCuckooCharacterMovementComponent>(GetMovementComponent());
	movementComponent->MaxWalkSpeed *= 2.0f;
}

void ACuckooCharacter::StopSprint()
{
	bSprinting = false;
	auto movementComponent = Cast<UCuckooCharacterMovementComponent>(GetMovementComponent());
	movementComponent->MaxWalkSpeed /= 2.0f;
}

void ACuckooCharacter::FireRewind()
{
	if (RewindCooldown > 0.0f) return;
	RewindCooldown = RewindCooldownDuration;
	StartRewind();
}

void ACuckooCharacter::StartRewind()
{
	GetWorldTimerManager().PauseTimer(RecordMovementStateHandle);
	GetController()->SetIgnoreMoveInput(true);
	GetController()->SetIgnoreLookInput(true);
	bRewinding = true;
	_rewindTime = 0.0f;
	_lastFrameNumber = 0;
	SetActorEnableCollision(false);
}

void ACuckooCharacter::StopRewind()
{
	GetWorldTimerManager().UnPauseTimer(RecordMovementStateHandle);
	GetController()->SetIgnoreMoveInput(false);
	GetController()->SetIgnoreLookInput(false);
	bRewinding = false;
	SetActorEnableCollision(true);
}
