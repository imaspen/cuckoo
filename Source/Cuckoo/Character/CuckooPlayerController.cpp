// Fill out your copyright notice in the Description page of Project Settings.


#include "CuckooPlayerController.h"

#include <Engine/GameViewportClient.h>
#include <Engine/LocalPlayer.h>

#include "CuckooCharacter.h"

#define GET_CHAR_SAFE auto character = Cast<ACuckooCharacter>(GetCharacter()); if (!character) return;

void ACuckooPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	bInvertLook = false;

	SetInputMode(FInputModeGameOnly::FInputModeGameOnly());

	InputComponent->BindAxis("MoveX", this, &ACuckooPlayerController::MoveX);
	InputComponent->BindAxis("MoveY", this, &ACuckooPlayerController::MoveY);
	
	InputComponent->BindAxis("Pitch", this, &ACuckooPlayerController::AddPitchInput);
	InputComponent->BindAxis("Yaw", this, &ACuckooPlayerController::AddYawInput);

	InputComponent->BindAction("Jump", IE_Pressed, this, &ACuckooPlayerController::StartJump);
	InputComponent->BindAction("Jump", IE_Released, this, &ACuckooPlayerController::StopJump);

	InputComponent->BindAction("Peck", IE_Pressed, this, &ACuckooPlayerController::Peck);

	InputComponent->BindAction("Sprint", IE_Pressed, this, &ACuckooPlayerController::Sprint);

	InputComponent->BindAction("Rewind", IE_Pressed, this, &ACuckooPlayerController::Rewind);

	InputComponent->BindAction("InvertLook", IE_Pressed, this, &ACuckooPlayerController::InvertLook);
}

void ACuckooPlayerController::BeginPlay()
{
	auto localPlayer = GetLocalPlayer();
	if (!localPlayer) return;
	localPlayer->ViewportClient->SetDisableSplitscreenOverride(false);
}

void ACuckooPlayerController::MoveX(float Amount)
{
	FVector direction = FRotationMatrix(GetControlRotation()).GetScaledAxis(EAxis::Y);
	direction.Z = 0;
	direction.Normalize();
	GET_CHAR_SAFE;
	character->AddMovementInput(direction, Amount);
}

void ACuckooPlayerController::MoveY(float Amount)
{
	FVector direction = FRotationMatrix(GetControlRotation()).GetScaledAxis(EAxis::X);
	direction.Z = 0;
	direction.Normalize();
	GET_CHAR_SAFE;
	character->AddMovementInput(direction, Amount);
}

void ACuckooPlayerController::AddPitchInput(float Amount)
{
	float ControlPitch = FMath::Abs(GetControlRotation().Pitch);
	if (ControlPitch > 180.0f) ControlPitch -= 360.0f;
	RotationInput.Pitch +=
		!IsLookInputIgnored()
		&& (ControlPitch < 80.0f || Amount > 0.0f)
		&& (ControlPitch > -80.0f || Amount < 0.0f)
		? (bInvertLook ? -1.0f : 1.0f) * Amount * InputPitchScale 
		: 0.f;
}

void ACuckooPlayerController::StartJump()
{
	GET_CHAR_SAFE;
	character->bPressedJump = true;
}

void ACuckooPlayerController::StopJump()
{
	GET_CHAR_SAFE;
	character->bPressedJump = false;
}

void ACuckooPlayerController::Peck()
{
	GET_CHAR_SAFE;
	character->Peck();
}

void ACuckooPlayerController::Sprint()
{
	GET_CHAR_SAFE;
	character->ToggleSprint();
}

void ACuckooPlayerController::Rewind()
{
	GET_CHAR_SAFE;
	character->FireRewind();
}

void ACuckooPlayerController::InvertLook()
{
	bInvertLook = !bInvertLook;
}
