// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "CuckooPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class CUCKOO_API ACuckooPlayerController : public APlayerController
{
	GENERATED_BODY()
	
protected:
	UFUNCTION()
	virtual void SetupInputComponent() override;
	
	UFUNCTION()
	virtual void BeginPlay() override;
	
private:
	void MoveX(float Amount);
	UFUNCTION()
	void MoveY(float Amount);

	UFUNCTION()
	void StartJump();
	UFUNCTION()
	void StopJump();

	UFUNCTION()
	void Peck();

	UFUNCTION()
	void Sprint();

	UFUNCTION()
	void Rewind();

	UFUNCTION()
	void InvertLook();

	virtual void AddPitchInput(float Amount) override;
	
	bool bInvertLook;
};
