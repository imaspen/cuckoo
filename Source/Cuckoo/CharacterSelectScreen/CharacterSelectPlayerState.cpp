// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacterSelectPlayerState.h"

#include <Engine/GameInstance.h>
#include <Engine/World.h>
#include <GameFramework/GameStateBase.h>

#include "CuckooGameInstance.h"
#include "CharacterSelectPlayerController.h"

void ACharacterSelectPlayerState::SetHoveredCharacter(uint8 NewHoveredCharacter)
{
	if (bConfirmed) return;
	HoveredCharacter = NewHoveredCharacter;
	while (HoveredCharacter < 0) HoveredCharacter += 4;
	while (HoveredCharacter >= 4) HoveredCharacter -= 4;
}

void ACharacterSelectPlayerState::SetConfirmed(bool bNewConfirmed)
{
	if (!bNewConfirmed) bConfirmed = false;

	auto world = GetWorld();
	if (!world) return;

	auto states = world->GetGameState()->PlayerArray;
	for (auto & otherState : states)
	{
		auto selectState = Cast<ACharacterSelectPlayerState>(otherState);
		if (!selectState) continue;
		if (selectState == this) continue;
		if (selectState->bConfirmed
			&& selectState->HoveredCharacter == HoveredCharacter) return;
	}

	auto instance = world->GetGameInstance<UCuckooGameInstance>();
	if (instance->CharacterSelections.Num() == 0) 
		instance->CharacterSelections.SetNumZeroed(4, false);
	instance->CharacterSelections[Index] = HoveredCharacter;
	bConfirmed = bNewConfirmed;
}
