// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacterSelectGameModeBase.h"

#include <Kismet/GameplayStatics.h>
#include <Engine/Engine.h>
#include <Engine/World.h>

#include "CharacterSelectGameStateBase.h"
#include "CharacterSelectPlayerState.h"

ACharacterSelectGameModeBase::ACharacterSelectGameModeBase()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ACharacterSelectGameModeBase::BeginPlay()
{
	Super::BeginPlay();

	for (int i = 0; i < 3; i++) UGameplayStatics::CreatePlayer(GetWorld(), i + 1);
}

void ACharacterSelectGameModeBase::Tick(float DeltaTime)
{
	auto state = GetGameState<ACharacterSelectGameStateBase>();
	if (!state) return;
	for (auto & playerState : state->PlayerArray)
	{
		auto selectState = Cast<ACharacterSelectPlayerState>(playerState);
		if (!selectState) return;
		if (!selectState->bConfirmed) return;
	}
	GetWorld()->ServerTravel(TEXT("/Game/Maps/Clock2"));
}

void ACharacterSelectGameModeBase::ForceStart()
{
	auto state = GetGameState<ACharacterSelectGameStateBase>();
	if (!state) return;
	int i = 0;
	for (auto & playerState : state->PlayerArray)
	{
		auto selectState = Cast<ACharacterSelectPlayerState>(playerState);
		if (!selectState) return;
		selectState->SetHoveredCharacter(i++);
		selectState->SetConfirmed(true);
	}
}