// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacterSelectPlayerController.h"

#include <Engine/GameViewportClient.h>
#include <Engine/LocalPlayer.h>

#include "CharacterSelectGameModeBase.h"
#include "CharacterSelectPlayerState.h"
#include "Engine/World.h"

void ACharacterSelectPlayerController::Tick(float DeltaTime)
{
}

void ACharacterSelectPlayerController::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	auto localPlayer = GetLocalPlayer();
	if (!localPlayer) return;
	GetLocalPlayer()->ViewportClient->SetDisableSplitscreenOverride(false);
}

void ACharacterSelectPlayerController::BeginPlay()
{
	auto localPlayer = GetLocalPlayer();
	if (!localPlayer) return;
	localPlayer->ViewportClient->SetDisableSplitscreenOverride(true);

	SetInputMode(FInputModeGameAndUI::FInputModeGameAndUI());
	
	auto state = GetPlayerState<ACharacterSelectPlayerState>();
	if (!state) return;
	state->Index = GetInputIndex();
}

void ACharacterSelectPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAction("Left", IE_Pressed, this, &ACharacterSelectPlayerController::Left);
	InputComponent->BindAction("Right", IE_Pressed, this, &ACharacterSelectPlayerController::Right);
	
	InputComponent->BindAction("Confirm", IE_Pressed, this, &ACharacterSelectPlayerController::Confirm);
	InputComponent->BindAction("Cancel", IE_Pressed, this, &ACharacterSelectPlayerController::Cancel);
	InputComponent->BindAction("ForceStart", IE_Pressed, this, &ACharacterSelectPlayerController::ForceStart);
}

void ACharacterSelectPlayerController::Left()
{
	auto state = GetPlayerState<ACharacterSelectPlayerState>();
	if (!state) return;
	state->SetHoveredCharacter(state->HoveredCharacter - 1);
}

void ACharacterSelectPlayerController::Right()
{
	auto state = GetPlayerState<ACharacterSelectPlayerState>();
	if (!state) return;
	state->SetHoveredCharacter(state->HoveredCharacter + 1);
}

void ACharacterSelectPlayerController::Confirm()
{
	auto ourState = GetPlayerState<ACharacterSelectPlayerState>();
	if (!ourState) return;
	ourState->SetConfirmed(true);
}

void ACharacterSelectPlayerController::Cancel()
{
	auto state = GetPlayerState<ACharacterSelectPlayerState>();
	if (!state) return;
	state->SetConfirmed(false);
}

void ACharacterSelectPlayerController::ForceStart()
{
	const auto World = GetWorld();
	if (!World) return;
	const auto GameMode = World->GetAuthGameMode<ACharacterSelectGameModeBase>();
	if (!GameMode) return;
	GameMode->ForceStart();
}