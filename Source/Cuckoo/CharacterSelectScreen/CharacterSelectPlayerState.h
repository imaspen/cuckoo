// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "CharacterSelectPlayerState.generated.h"

/**
 * 
 */
UCLASS()
class CUCKOO_API ACharacterSelectPlayerState : public APlayerState
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	uint8 HoveredCharacter;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bConfirmed;

	UFUNCTION(BlueprintSetter)
	virtual void SetHoveredCharacter(uint8 NewHoveredCharacter);

	UFUNCTION(BlueprintSetter)
	virtual void SetConfirmed(bool bNewConfirmed);

	uint8 Index;
};
