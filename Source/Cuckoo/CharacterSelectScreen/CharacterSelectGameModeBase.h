// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "CharacterSelectGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class CUCKOO_API ACharacterSelectGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
public:
	ACharacterSelectGameModeBase();

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;
	void ForceStart();
};
