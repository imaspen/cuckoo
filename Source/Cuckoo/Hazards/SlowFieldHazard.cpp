// Fill out your copyright notice in the Description page of Project Settings.


#include "SlowFieldHazard.h"
#include "Cuckoo/Character/CuckooCharacter.h"
#include "Cuckoo/Character/CuckooCharacterMovementComponent.h"
#include <Components/ShapeComponent.h>

// Sets default values
ASlowFieldHazard::ASlowFieldHazard() 
{
	GetCollisionComponent()->OnComponentBeginOverlap.AddDynamic(this, &ASlowFieldHazard::OnOverlapBegin);
	GetCollisionComponent()->OnComponentEndOverlap.AddDynamic(this, &ASlowFieldHazard::OnOverlapEnd);
}

// Called when the game starts or when spawned
void ASlowFieldHazard::BeginPlay()
{
	Super::BeginPlay();
	
}

void ASlowFieldHazard::OnOverlapBegin(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	ACuckooCharacter * player = Cast<ACuckooCharacter, AActor>(OtherActor);
	
	if (!player) return;

	auto movementComponent = Cast<UCuckooCharacterMovementComponent>(player->GetMovementComponent());

	movementComponent->MaxWalkSpeed = (movementComponent->MaxWalkSpeed / SpeedModifier);
}

void ASlowFieldHazard::OnOverlapEnd(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex)
{
	ACuckooCharacter * player = Cast<ACuckooCharacter, AActor>(OtherActor);

	if (!player) return;

	auto movementComponent = Cast<UCuckooCharacterMovementComponent>(player->GetMovementComponent());

	movementComponent->MaxWalkSpeed = (movementComponent->MaxWalkSpeed * SpeedModifier);
}

