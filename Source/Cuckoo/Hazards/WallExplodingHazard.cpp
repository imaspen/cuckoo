
#include "WallExplodingHazard.h"
#include "Cuckoo/Character/CuckooCharacter.h"
#include <Components/ShapeComponent.h>


AWallExplodingHazard::AWallExplodingHazard()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AWallExplodingHazard::Tick(float DeltaTime)
{
	time++;
	if (time >= RefreshRate) {
		ReleaseDebris();
	}
	if (time >= RefreshRate + WallExplosionLength) {
		time = 0.0;
	}

}

void AWallExplodingHazard::ReleaseDebris()
{
	TSet<AActor *> overlappingActors;
	GetCollisionComponent()->GetOverlappingActors(
		overlappingActors, TSubclassOf<ACuckooCharacter>()
	);

	for (auto otherPlayer : overlappingActors) {
		auto direction = otherPlayer->GetActorLocation() - GetActorLocation();
		direction.Normalize();

		auto cuckoo = Cast<ACuckooCharacter>(otherPlayer);
		cuckoo->LaunchCharacter(direction * WallExplosionForce, false, false);
	}
}