// Fill out your copyright notice in the Description page of Project Settings.


#include "ClockHandHazard.h"

// Sets default values
AClockHandHazard::AClockHandHazard()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>("RootComponent");
	HandSpeedModifier = 1.0f;
	Apex = 45.0f;
}

// Called when the game starts or when spawned
void AClockHandHazard::BeginPlay()
{
	Super::BeginPlay();
	_time = 0.0f;
}

// Called every frame
void AClockHandHazard::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	_time += DeltaTime;

	float rotation = FMath::Sin(_time * HandSpeedModifier) * Apex * 0.01745329252f;

	(FQuat(FVector(0.0f, 1.0f, 0.0f), rotation), true);
}


