// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerBox.h"
#include "FallingDebrisHazard.generated.h"

/**
 * 
 */
UCLASS()
class CUCKOO_API AFallingDebrisHazard : public ATriggerBox
{
	GENERATED_BODY()
	

public:
	AFallingDebrisHazard();
	virtual void Tick(float DeltaTime) override;

	virtual void ReleaseDebris();

	UPROPERTY(EditAnywhere)
		float RefreshRate;
	UPROPERTY(EditAnywhere)
		float DebrisDropLength;
 float time;
};
