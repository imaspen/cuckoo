// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerBox.h"
#include "WallExplodingHazard.generated.h"

/**
 * 
 */
UCLASS()
class CUCKOO_API AWallExplodingHazard : public ATriggerBox
{
	GENERATED_BODY()

public:

AWallExplodingHazard();
 virtual void Tick(float DeltaTime) override;
 virtual void ReleaseDebris();
 UPROPERTY(EditAnywhere)
float RefreshRate;

 UPROPERTY(EditAnywhere)
	 float WallExplosionLength;
 
 UPROPERTY(EditAnywhere)
	 float WallExplosionForce;
	
 float time;
};
