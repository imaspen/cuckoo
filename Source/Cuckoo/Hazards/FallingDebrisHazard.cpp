// Fill out your copyright notice in the Description page of Project Settings.


#include "FallingDebrisHazard.h"
#include "Cuckoo/Character/CuckooCharacter.h"
#include <Components/ShapeComponent.h>


AFallingDebrisHazard::AFallingDebrisHazard() 
{
	PrimaryActorTick.bCanEverTick = true;
}

void AFallingDebrisHazard::Tick(float DeltaTime) 
{
	time++;
	if (time >= RefreshRate) {
		ReleaseDebris();
	}
	if (time >= RefreshRate + DebrisDropLength) {
		time = 0.0;
	}

}

void AFallingDebrisHazard::ReleaseDebris()
{
	TSet<AActor *> overlappingActors;
	GetCollisionComponent()->GetOverlappingActors(
		overlappingActors, TSubclassOf<ACuckooCharacter>()
	);
	
	for (auto otherPlayer : overlappingActors) {
		otherPlayer->Destroy();
	}
}

