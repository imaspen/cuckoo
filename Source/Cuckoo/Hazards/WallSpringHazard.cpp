// Fill out your copyright notice in the Description page of Project Settings.
#include "WallSpringHazard.h"

#include "Components/StaticMeshComponent.h"

// Sets default values
AWallSpringHazard::AWallSpringHazard()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>("RootComponent");
	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("StaticMeshComponent");
	StaticMeshComponent->SetupAttachment(RootComponent);

	ExtendDuration = 0.25f;
	RetractDuration = 0.5f;
	ExtendedWaitDuration = 1.0f;
	RetractedWaitDuration = 1.0f;
	ExtensionScale = 2.0f;
}

// Called when the game starts or when spawned
void AWallSpringHazard::BeginPlay()
{
	Super::BeginPlay();
	DefaultScale = GetActorRelativeScale3D();
}

// Called every frame
void AWallSpringHazard::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	_runningTime = FMath::Fmod((_runningTime + DeltaTime), (RetractedWaitDuration + ExtendDuration + ExtendedWaitDuration + RetractDuration));
	
	//FVector offset = FVector(GetActorSc1ale().X, SpringForce, GetActorScale().Z);
	auto newScale = FVector(DefaultScale);


	if (_runningTime <= RetractedWaitDuration)
	{
	}
	else if (_runningTime <= RetractedWaitDuration + ExtendDuration)
	{
		newScale.Y *= FMath::Lerp(1.0f, ExtensionScale, (_runningTime - RetractedWaitDuration) / ExtendDuration);
	}
	else if (_runningTime <= RetractedWaitDuration + ExtendDuration + ExtendedWaitDuration)
	{
		newScale.Y *= ExtensionScale;
	}
	else
	{
		newScale.Y *= FMath::Lerp(ExtensionScale, 1.0f, (_runningTime - RetractedWaitDuration - ExtendDuration - ExtendedWaitDuration) / RetractDuration);
	}

	SetActorRelativeScale3D(newScale);
}

