// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerBox.h"
#include "SlowFieldHazard.generated.h"

UCLASS()
class CUCKOO_API ASlowFieldHazard : public ATriggerBox
{
	GENERATED_BODY()
	
protected:

// Called when the game starts or when spawned
virtual void BeginPlay() override;

public:	
// Sets default values for this actor's properties
ASlowFieldHazard();

UFUNCTION()
void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

UFUNCTION()
void OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

UPROPERTY(EditAnywhere)
float SpeedModifier; 
};
