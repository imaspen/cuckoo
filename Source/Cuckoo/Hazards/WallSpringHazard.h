// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "WallSpringHazard.generated.h"

UCLASS()
class CUCKOO_API AWallSpringHazard : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWallSpringHazard();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	class UStaticMeshComponent * StaticMeshComponent;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float ExtendDuration;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float RetractDuration;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float ExtendedWaitDuration;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float RetractedWaitDuration;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float ExtensionScale;

private:
	FVector DefaultScale;
    float _runningTime;
    bool bGoBackwards = false;
};
