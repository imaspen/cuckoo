// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "CuckooGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class CUCKOO_API UCuckooGameInstance : public UGameInstance
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<uint8> CharacterSelections;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	uint8 Winner;
};
