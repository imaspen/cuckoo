// Fill out your copyright notice in the Description page of Project Settings.


#include "CuckooModule.h"

#include "Modules/ModuleManager.h"
#include "Logging/LogMacros.h"
#include "Misc/Paths.h"

void FCuckooModule::StartupModule()
{
	FString ShaderDirectory = FPaths::Combine(FPaths::ProjectDir(), TEXT("Shaders"));
	AddShaderSourceDirectoryMapping("/Project", ShaderDirectory);
}

void FCuckooModule::ShutdownModule()
{
	ResetAllShaderSourceDirectoryMappings();
}